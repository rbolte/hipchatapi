package com.hipchat.api.v1;

import java.lang.reflect.Type;

public interface HipChatApi {

    
    // ----------------------------------------------------------------------------------------------- Statics

    public static enum Method { GET, POST };
    
    // ----------------------------------------------------------------------------------------------- Getters & Setters

    public Error getLastError();
    
    public void setLastError(Error lastError);
    public Exception getLastErrorException();
    public void setLastErrorException(Exception lastErrorException);
    
    public Rooms getRooms();
    public Users getUsers();
    public String getScheme();
    public void setScheme(String scheme);
    public String getHost();
    public void setHost(String host);
    public String getAuthToken();
    public void setAuthToken(String authToken);
    public String getDefaultTitle();
    public void setDefaultTitle(String defaultTitle);
    public String getDefaultPassword();
    public void setDefaultPassword(String defaultPassword);
    public boolean isCachingEnabled();
    public void setCachingEnabled(boolean cachingEnabled);
    
    // ----------------------------------------------------------------------------------------------- Public Functions

    public boolean execute(String path);
    public boolean execute(String path, HipChatApi.Method method);
    public boolean execute(String path, String data);
    public boolean execute(String path, String data, HipChatApi.Method method);
    
    public String getJson(String path);
    public String getJson(String path, HipChatApi.Method method);
    public String getJson(String path, String data);
    public String getJson(String path, String data, HipChatApi.Method method);

    public <T> T getObject(String path, Class<T> type);
    public <T> T getObject(String path, String data, Class<T> type);
    public <T> T getObject(String path, Class<T> type, String rootElement);
    public <T> T getObject(String path, HipChatApi.Method method, Class<T> type, String rootElement);
    public <T> T getObject(String path, String data, Class<T> type, String rootElement);
    public <T> T getObject(String path, String data, HipChatApi.Method method, Class<T> type, String rootElement);
    public <T> T getObject(String path, Type type);
    public <T> T getObject(String path, String data, Type type);
    public <T> T getObject(String path, Type type, String rootElement);
    public <T> T getObject(String path, HipChatApi.Method method, Type type, String rootElement);
    public <T> T getObject(String path, String data, Type type, String rootElement);
    public <T> T getObject(String path, String data, HipChatApi.Method method, Type type, String rootElement);
    
    public void flushCache();
    
    // ----------------------------------------------------------------------------------------------- Public methods


    // ----------------------------------------------------------------------------------------------- Private methods


    // ----------------------------------------------------------------------------------------------- Private Getters & Setters

}
