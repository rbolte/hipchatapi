
package com.hipchat.api.v1;

import java.util.Date;
import java.util.List;

public interface Room {
    
    public static enum Action { Create, Delete, History, Message, Topic, Show };
    public static enum MessageFormat { text, html };
    public static enum MessageColor { yellow, red, green, purple, gray, random };

    public int getId();
    public void setId(int id);
    public String getName();
    public void setName(String name);
    public String getTopic();
    public void setTopic(String topic);
    public Date getLastActive();
    public void setLastActive(Date lastActive);
    public Date getCreated();
    public void setCreated(Date created);
    public int getOwnerId();
    public void setOwnerId(int ownerId);
    public Boolean isArchivedRoom();
    public void setArchived(Boolean archivedRoom);
    public Boolean isPrivateRoom();
    public void setPrivate(Boolean privateRoom);
    public String getXmppJId();
    public void setXmppJId(String xmppJId);
    public Boolean isAllowGuests();
    public void setAllowGuests(Boolean allowGuests);
    public String getGuestAccessUrl();
    public void setGuestAccessUrl(String guestAccessUrl);
    public List<Integer> getMemberIds();
    public List<UserImpl> getMembers();
    public List<UserImpl> getParticipants();
    public void setParticipants(List<UserImpl> participants);
    public List<Message> getHistory();
    public List<Message> getHistory(String date, String timezone);
    
    public boolean create();
    public boolean update();
    public boolean delete();
    public boolean sendMessage(String author, String message);
    public boolean sendMessage(String author, String message, MessageFormat format);
    public boolean sendMessage(String author, String message, MessageFormat format, boolean notify);    
    public boolean sendMessage(String author, String message, MessageFormat format, boolean notify, MessageColor color);
    public String toString(Action action);
}
