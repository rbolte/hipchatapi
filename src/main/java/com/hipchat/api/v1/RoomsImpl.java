
package com.hipchat.api.v1;

import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

public class RoomsImpl implements Rooms {
    
    protected RoomsImpl() {
        
    }
    
    @Override
    public Room get(int id) {
        return getInstance().getObject("rooms/show", String.format("room_id=%s", id), RoomImpl.class, "room");
    }
    
    @Override
    public List<Room> getList() {
        return getInstance().getObject("rooms/list", new TypeToken<ArrayList<RoomImpl>>() {}.getType(), "rooms");
    }
        
    @Override
    public boolean create(String name, int ownerId) {
        return create(name, ownerId, "");
    }
    
    @Override
    public boolean create(String name, int ownerId, String topic) {
        return create(name, ownerId, topic, true);
    }
    
    @Override
    public boolean create(String name, int ownerId, String topic, boolean privateRoom) {
        return create(name, ownerId, topic, privateRoom, false);
    }
    
    @Override
    public boolean create(String name, int ownerId, String topic, boolean privateRoom, boolean allowGuests) {
        RoomImpl room = new RoomImpl(name, ownerId, topic, privateRoom, allowGuests);
        return room.create();
    }

    @Override
    public boolean delete(int id) {
        Room room = get(id);
        return room.delete();
    }
    
    private HipChatApi getInstance() {
        return HipChatApiImpl.getInstance();
    }

    
}
