
package com.hipchat.api.v1;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.hipchat.api.v1.HipChatApi.Method;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class RoomImpl implements Room {
    
    @SerializedName("room_id")          private int id;
    @SerializedName("name")             private String name;
    @SerializedName("topic")            private String topic;
    @SerializedName("last_active")      private Date lastActive;
    @SerializedName("created")          private Date created;
    @SerializedName("owner_user_id")    private int ownerId;
    @SerializedName("is_archived")      private Boolean archivedRoom;
    @SerializedName("is_private")       private Boolean privateRoom;
    @SerializedName("xmpp_jid")         private String xmppJId;
    @SerializedName("guest_access_url") private String guestAccessUrl;
    @SerializedName("member_user_ids")  private List<Integer> memberIds;
    @SerializedName("participants")     private List<UserImpl> participants;
    private Boolean allowGuests;

    protected RoomImpl() {
        
    }
    
    public RoomImpl(String name, int ownerId, String topic, boolean privateRoom, boolean allowGuests) {
        this.name = name;
        this.ownerId = ownerId;
        this.topic = topic;
        this.privateRoom = privateRoom;
        this.allowGuests = allowGuests;
    }
    
    // ----------------------------------------------------------------------------------------------- Getters & Setters
    
    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTopic() {
        return topic;
    }

    @Override
    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public Date getLastActive() {
        return lastActive;
    }

    @Override
    public void setLastActive(Date lastActive) {
        this.lastActive = lastActive;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public int getOwnerId() {
        return ownerId;
    }

    @Override
    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public Boolean isArchivedRoom() {
        return archivedRoom;
    }

    @Override
    public void setArchived(Boolean archivedRoom) {
        this.archivedRoom = archivedRoom;
    }

    @Override
    public Boolean isPrivateRoom() {
        return privateRoom;
    }

    @Override
    public void setPrivate(Boolean privateRoom) {
        this.privateRoom = privateRoom;
    }

    @Override
    public String getXmppJId() {
        return xmppJId;
    }

    @Override
    public void setXmppJId(String xmppJId) {
        this.xmppJId = xmppJId;
    }

    @Override
    public Boolean isAllowGuests() {
        return (guestAccessUrl != null);
    }

    @Override
    public void setAllowGuests(Boolean allowGuests) {
        this.allowGuests = allowGuests;
    }

    @Override
    public String getGuestAccessUrl() {
        return guestAccessUrl;
    }

    @Override
    public void setGuestAccessUrl(String guestAccessUrl) {
        this.guestAccessUrl = guestAccessUrl;
    }

    @Override
    public List<Integer> getMemberIds() {
        return memberIds;
    }
    
    @Override
    public List<UserImpl> getMembers() {
        ArrayList<UserImpl> result = new ArrayList<UserImpl>();
        if(memberIds == null) { lazyload(); }

        if(memberIds != null) {
            for(int memberId : memberIds) {
                result.add(new UserImpl(memberId));
            }
        }
        
        return result;
    }
    
    @Override
    public List<UserImpl> getParticipants() {
        if(participants == null) { lazyload(); }
        return participants;
    }

    @Override
    public void setParticipants(List<UserImpl> participants) {
        this.participants = participants;
    }

    @Override
    public List<Message> getHistory() {
        return getHistory("recent", "UTC");
    }
    
    @Override
    public List<Message> getHistory(String date, String timezone) {
        
        ArrayList<Message> result = new ArrayList<Message>();
        String response = getInstance().getJson("rooms/history", String.format(toString(Action.History), date, timezone));
        
        if(response != null && !response.equals("")) {
            Gson gson = new Gson();
            Map<String,Object> objects = gson.fromJson(response, Map.class);
            String json = gson.toJson(objects.get("messages"));
            result = gson.fromJson(json, new TypeToken<ArrayList<Message>>() {}.getType());
        }
        
        return result;
    }
    
    // ----------------------------------------------------------------------------------------------- Public Methods
    
    @Override
    public boolean create() {
        return getInstance().execute("rooms/create", toString(Action.Create), Method.POST);
    }

    @Override
    public boolean update() {
        return getInstance().execute("rooms/topic", String.format(toString(Action.Topic), topic, ownerId), Method.POST);
    }
    
    @Override
    public boolean delete() {
        return getInstance().execute("rooms/delete", toString(Action.Delete), Method.POST);
    }
    
    @Override
    public boolean sendMessage(String author, String message) {
        return sendMessage(author, message, MessageFormat.text);
    }
    @Override
    public boolean sendMessage(String author, String message, MessageFormat format) {
        return sendMessage(author, message, format, false);
    }
    @Override
    public boolean sendMessage(String author, String message, MessageFormat format, boolean notify) {
        return sendMessage(author, message, format, notify, MessageColor.yellow);
    }
    @Override
    public boolean sendMessage(String author, String message, MessageFormat format, boolean notify, MessageColor color) {
        String data = String.format(toString(Action.Message), author, message, format.toString(), (notify ? "1" : "0"), color.toString());
        return getInstance().execute("rooms/message", data, Method.POST);
    }
        
    @Override
    public String toString(Action action) {
        
        switch(action) {
                            
            case Delete:
                return getQueryStringWithOnlyRoomId();
                
            case History:
                return getQueryStringForHistory();
                
            case Message:
                return getQueryStringForMessage();
                
            case Topic:
                return getQueryStringForTopic();

            case Show:
                return getQueryStringWithOnlyRoomId();
            
            case Create:
            default:
                return getQueryStringForCreation();

        }
    }

    // ----------------------------------------------------------------------------------------------- Private Methods
    
    private static HipChatApi getInstance() {
        return HipChatApiImpl.getInstance();
    }
    
    private void lazyload() {

        Room lazyload = getInstance().getObject("rooms/show", getQueryStringWithOnlyRoomId(), RoomImpl.class, "room");
        if(lazyload != null && (lazyload.getId() == getId())) {
            this.name= lazyload.getName();
            this.topic = lazyload.getTopic();
            this.lastActive = lazyload.getLastActive();
            this.created = lazyload.getCreated();
            this.archivedRoom = lazyload.isArchivedRoom();
            this.privateRoom = lazyload.isPrivateRoom();
            this.ownerId = lazyload.getOwnerId();
            this.memberIds = lazyload.getMemberIds();
            this.participants = lazyload.getParticipants();
            this.guestAccessUrl = lazyload.getGuestAccessUrl();
            this.xmppJId = lazyload.getXmppJId();
        }
        
    }
    
    private String getQueryStringForCreation() throws IllegalArgumentException {
        // Throwing unchecked runtime exceptions on purpose:
        // http://tutorials.jenkov.com/java-exception-handling/checked-or-unchecked-exceptions.html
        IllegalArgumentException exception = null;
        if (name == null || name.length() == 0) {
            exception = new IllegalArgumentException(Error.REQUIRED_PARAMETER_NAME);
        } else if (ownerId <= 0) {
            exception = new IllegalArgumentException(Error.REQUIRED_PARAMETER_OWNERID);
        }
        
        StringBuilder qs = new StringBuilder();
        try {
            qs.append(String.format("name=%s", URLEncoder.encode(name, "UTF-8")));
            qs.append(String.format("&owner_user_id=%s", ownerId));
            qs.append(String.format("&privacy=%s", URLEncoder.encode((privateRoom ? "private" : "public"), "UTF-8")));
            qs.append(String.format("&topic=%s", URLEncoder.encode(topic, "UTF-8")));
            qs.append(String.format("&guest_access=%s", (allowGuests ? "1" : "0")));
        } catch(UnsupportedEncodingException e) {
            exception = new IllegalArgumentException(Error.GENERIC_ERROR_MESSAGE, e);
        }

        if(exception != null) {
            getInstance().setLastErrorException(exception);
            throw exception;
        }
        
        return qs.toString();
    }
    
    private String getQueryStringForHistory() {
        StringBuilder qs = new StringBuilder();
        qs.append(getQueryStringWithOnlyRoomId());
        qs.append("&date=%s");
        qs.append("&timezone=%s");
        return qs.toString();
    }
    
    private String getQueryStringForMessage() {
        StringBuilder qs = new StringBuilder();
        qs.append(getQueryStringWithOnlyRoomId());
        qs.append("&from=%s");
        qs.append("&message=%s");
        qs.append("&message_format=%s");
        qs.append("&notify=%s");
        qs.append("&color=%s");
        return qs.toString();
    }
    
    private String getQueryStringForTopic() {
        StringBuilder qs = new StringBuilder();
        qs.append(getQueryStringWithOnlyRoomId());
        qs.append("&topic=%s");
        qs.append("&from=%s");
        return qs.toString();
        
    }
    
    private String getQueryStringWithOnlyRoomId() throws IllegalArgumentException {
        // Throwing an unchecked runtime exception on purpose:
        // http://tutorials.jenkov.com/java-exception-handling/checked-or-unchecked-exceptions.html
        IllegalArgumentException exception = null;
        if (id <= 0) {
            exception = new IllegalArgumentException(Error.REQUIRED_PARAMETER_ROOMID);
        }
        
        StringBuilder qs = new StringBuilder();
        qs.append(String.format("room_id=%s", id));

        if(exception != null) {
            getInstance().setLastErrorException(exception);
            throw exception;
        }
        
        return qs.toString();
    }
    
}
