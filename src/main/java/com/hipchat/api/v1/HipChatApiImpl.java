
package com.hipchat.api.v1;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.TimeZone;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

public enum HipChatApiImpl implements HipChatApi {

    INSTANCE;
        
    private final String VERSION = "v1";
    private final String CACHEKEY = "com.hipchat.api.v1";
    
    private String scheme = "https";
    private String host = "api.hipchat.com";
    private String authToken;
    private String defaultTitle = "";
    private String defaultPassword = "";
    private boolean cachingEnabled = false;
    
    private Error lastError;
    private Exception lastErrorException;
    
    // ----------------------------------------------------------------------------------------------- Getters & Setters

    private HipChatApiImpl() {
        if(isCachingEnabled()) {
            // Keep API results in cache for 5 minutes to avoid issues with request limit
            CacheManager cm = CacheManager.getInstance();
            if(!cm.cacheExists(CACHEKEY)) {
                cm.addCache(new Cache(CACHEKEY, 10, false, false, 300, 180));
            }
        }
    }
    
    public static HipChatApi getInstance() {
        return INSTANCE;
    }
    
    public Error getLastError() {
        return lastError;
    }
    
    public void setLastError(Error lastError) {
        this.lastError = lastError;
    }

    public Exception getLastErrorException() {
        return lastErrorException;
    }
    
    public void setLastErrorException(Exception lastErrorException) {
        this.lastErrorException = lastErrorException;
    }
    
    public Rooms getRooms() {
        return new RoomsImpl();
    }
    
    public Users getUsers() {
        return new UsersImpl();
    }
    
    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getDefaultTitle() {
        return defaultTitle;
    }

    public void setDefaultTitle(String defaultTitle) {
        this.defaultTitle = defaultTitle;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    public void setCachingEnabled(boolean cachingEnabled) {
        this.cachingEnabled = cachingEnabled;
    }

    // ----------------------------------------------------------------------------------------------- Public Functions

    public boolean execute(String path) {
        return execute(path, Method.GET);
    }
    
    public boolean execute(String path, Method method) {
        return execute(path, null, method);
    }

    public boolean execute(String path, String data) {
        return execute(path, data, Method.GET);
    }
    
    public boolean execute(String path, String data, Method method) {
        // Because execute statements (e.g. creating a group or user) can invalidate
        // the result of already cached room / user lists, the cache needs to be
        // flushed in order to provide acurate results. There should be a better
        // way of doing this (for instance, only flush room / user results.
        flushCache();

        String response = this.getJson(path, data, method);
        return (response != null);
    }
    
    public String getJson(String path) {
        return getJson(path, Method.GET);
    }

    public String getJson(String path, Method method) {
        return getJson(path, null, method);
    }
    
    public String getJson(String path, String data) {
        return getJson(path, data, Method.GET);
    }
    
    public String getJson(String path, String data, Method method) {

        if(isCachingEnabled()) {
            CacheManager cm = CacheManager.getInstance();
            String cacheElementKey = DigestUtils.md5Hex("JSON::" + path + ":" + data + ":" + method);
            Element element = cm.getCache(CACHEKEY).get(cacheElementKey);
            if(element != null) { 
                return (String) element.getObjectValue();
            }
        }
        
        HttpURLConnection connection = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        String result = "";

        try {

            if(method.equals(Method.GET) && (data != null && !data.isEmpty())) {
                if(path.contains("?")) {
                    path = String.format("%s&%s", path, data);
                } else {
                    path = String.format("%s?%s", path, data);
                }
            }

            URL requestUrl = new URL(getUrl(path));
            connection = (HttpURLConnection) requestUrl.openConnection();
            connection.setDoOutput(true);

            if(method.equals(Method.POST) && (data != null && !data.isEmpty())) {
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", Integer.toString(data.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");

                outputStream = new BufferedOutputStream(connection.getOutputStream());
                IOUtils.write(data, outputStream);
                IOUtils.closeQuietly(outputStream);
            }

            inputStream = connection.getInputStream();
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer);
            result = writer.toString();
        } catch (MalformedURLException ex) {
            lastError = null;
            lastErrorException = ex;
            return null;
        } catch (IOException ex) {
            try {
                BufferedReader errorReader = new BufferedReader(new InputStreamReader(connection.getErrorStream(), "UTF-8"));
                String errorResponse = errorReader.readLine();

                Gson gson = new GsonBuilder().create();
                JsonParser jsonParser = new JsonParser();
                JsonElement json = jsonParser.parse(errorResponse).getAsJsonObject().get("error");
                lastError = gson.fromJson(json, ErrorImpl.class);
                
                errorReader.close();
            } catch (NullPointerException npe) {
            } catch (IOException ioex) {
            }
    
            lastErrorException = ex;
            return null;
        } finally {
            if(connection != null) {
                connection.disconnect();
            }

            if(outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException ex) { }
            }

            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ex) { }
            }
        }

        lastError = null;
        lastErrorException = null;

        if(isCachingEnabled()) {
            CacheManager cm = CacheManager.getInstance();
            String cacheElementKey = DigestUtils.md5Hex("JSON::" + path + ":" + data + ":" + method);
            cm.getCache(CACHEKEY).put(new Element(cacheElementKey, result));
        }
        
        return result;
    }   

    public <T> T getObject(String path, Class<T> type) {
        return getObject(path, type, null);
    }

    public <T> T getObject(String path, String data, Class<T> type) {
        return getObject(path, data, type, null);
    }

    public <T> T getObject(String path, Class<T> type, String rootElement) {
        return getObject(path, Method.GET, type, rootElement);
    }

    public <T> T getObject(String path, Method method, Class<T> type, String rootElement) {
        return getObject(path, null, method, type, rootElement);
    }
    
    public <T> T getObject(String path, String data, Class<T> type, String rootElement) {
        return getObject(path, data, Method.GET, type, rootElement);
    }
    
    public <T> T getObject(String path, String data, Method method, Class<T> type, String rootElement) {
        
        if(isCachingEnabled()) {
            String cacheElementKey = DigestUtils.md5Hex("GSON::" + path + ":" + data + ":" + method);
            CacheManager cm = CacheManager.getInstance();
            Element element = cm.getCache(CACHEKEY).get(cacheElementKey);

            if(element != null) {
                return (T)element.getObjectValue();
            }
        }

        T result = null;
        String response = getJson(path, data, method);
        if(response != null && !response.isEmpty()) {

            Gson gson = new GsonBuilder()
                        .registerTypeAdapter(Boolean.class, TypeAdapters.booleanAsIntAdapter)
                        .registerTypeAdapter(boolean.class, TypeAdapters.booleanAsIntAdapter)
                        .registerTypeAdapter(Date.class, TypeAdapters.dateAsUnixTimestamp )
                        .registerTypeAdapter(TimeZone.class, TypeAdapters.timezoneAsStringAdapter)
                        .create();

            if(rootElement != null && !rootElement.isEmpty()) {
                JsonParser jsonParser = new JsonParser();
                JsonElement json = jsonParser.parse(response).getAsJsonObject().get(rootElement);
                result = gson.fromJson(json, type);
            } else {
                result = gson.fromJson(response, type);
            }
        }
            
        if(isCachingEnabled()) {
            if(result != null) {
                String cacheElementKey = DigestUtils.md5Hex("GSON::" + path + ":" + data + ":" + method);
                CacheManager cm = CacheManager.getInstance();
                cm.getCache(CACHEKEY).put(new Element(cacheElementKey, result));
            }
        }
            
        return result;
    }

    public <T> T getObject(String path, Type type) {
        return getObject(path, type, null);
    }

    public <T> T getObject(String path, String data, Type type) {
        return getObject(path, data, type, null);
    }

    public <T> T getObject(String path, Type type, String rootElement) {
        return getObject(path, Method.GET, type, rootElement);
    }

    public <T> T getObject(String path, Method method, Type type, String rootElement) {
        return getObject(path, null, method, type, rootElement);
    }
    
    public <T> T getObject(String path, String data, Type type, String rootElement) {
        return getObject(path, data, Method.GET, type, rootElement);
    }
    
    public <T> T getObject(String path, String data, Method method, Type type, String rootElement) {
        if(isCachingEnabled()) {
            String cacheElementKey = DigestUtils.md5Hex("GSON::" + path + ":" + data + ":" + method);
            CacheManager cm = CacheManager.getInstance();
            Element element = cm.getCache(CACHEKEY).get(cacheElementKey);
            if(element != null) {
                return (T)element.getObjectValue();
            }
        }

        T result = null;
        String response = getJson(path, data, method);
        if(response != null && !response.isEmpty()) {

            Gson gson = new GsonBuilder()
                        .registerTypeAdapter(Boolean.class, TypeAdapters.booleanAsIntAdapter)
                        .registerTypeAdapter(boolean.class, TypeAdapters.booleanAsIntAdapter)
                        .registerTypeAdapter(Date.class, TypeAdapters.dateAsUnixTimestamp )
                        .registerTypeAdapter(TimeZone.class, TypeAdapters.timezoneAsStringAdapter)
                        .create();

            if(rootElement != null && !rootElement.isEmpty()) {
                JsonParser jsonParser = new JsonParser();
                JsonElement json = jsonParser.parse(response).getAsJsonObject().get(rootElement);
                result = gson.fromJson(json, type);
            } else {
                result = gson.fromJson(response, type);
            }
        }
            
        if(isCachingEnabled()) {
            if(result != null) {
                String cacheElementKey = DigestUtils.md5Hex("GSON::" + path + ":" + data + ":" + method);
                CacheManager cm = CacheManager.getInstance();
                cm.getCache(CACHEKEY).put(new Element(cacheElementKey, result));
            }
        }
            
        return result;
    }
    
    public void flushCache() {
        if(isCachingEnabled()) {
            CacheManager cm = CacheManager.getInstance();
            cm.getCache(CACHEKEY).flush();
        }
    }
    
    
    // ----------------------------------------------------------------------------------------------- Private Functions
    
    private String getUrl(String path) {
        if(authToken == null && authToken.isEmpty()) {
            // Throwing an unchecked runtime exception on purpose:
            // http://tutorials.jenkov.com/java-exception-handling/checked-or-unchecked-exceptions.html
            this.setLastErrorException(new IllegalStateException(Error.REQUIRED_PARAMETER_AUTHTOKEN));
            throw (IllegalStateException) getLastErrorException();
        }
        
        if(!path.toLowerCase().contains("auth_token")) {
            if(path.contains("?")) {
                path += "&auth_token=" + authToken;
            } else {
                path += "?auth_token=" + authToken;
            }
        }
        
        if(!path.toLowerCase().contains("format")) {
            if(path.contains("?")) {
                path += "&format=json";
            } else {
                path += "?format=json";
            }
        }
        
        return String.format("%s/%s", getServerAddress(), path);
    }
    
    private String getServerAddress() {
        return String.format("%s://%s/%s", scheme, host, VERSION);
    }
    
        
}
