
package com.hipchat.api.v1;

import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class UsersImpl implements Users {

    // ----------------------------------------------------------------------------------------------- Public Methods
    
    @Override
    public User get(int id) {
        return getConnection().getObject("users/show", String.format("user_id=%s", id), HipChatApi.Method.GET, UserImpl.class, "user");
    }

    @Override
    public List<User> getList() {
        return getList(false);
    }

    @Override
    public List<User> getList(boolean includeDeleted) {
        return getConnection().getObject("users/list", String.format("include_deleted=%s", (includeDeleted ? "1" : "0")), HipChatApi.Method.GET, new TypeToken<ArrayList<UserImpl>>() {}.getType(), "users");
    }
    
    @Override
    public User create(String emailAddress, String name) {
        return create(emailAddress, name, name.replaceAll("\\s+",""));
    }

    @Override
    public User create(String emailAddress, String name, String mentionName) {
        return create(emailAddress, name, mentionName, getConnection().getDefaultTitle());
    }

    @Override
    public User create(String emailAddress, String name, String mentionName, String title) {
        return create(emailAddress, name, mentionName, title, false);
    }

    @Override
    public User create(String emailAddress, String name, String mentionName, String title, boolean admin) {
        return create(emailAddress, name, mentionName, title, false, getConnection().getDefaultPassword());
    }

    @Override
    public User create(String emailAddress, String name, String mentionName, String title, boolean admin, String password) {
        return create(emailAddress, name, mentionName, title, admin, password, null);
    }

    @Override
    public User create(String emailAddress, String name, String mentionName, String title, boolean admin, String password, TimeZone timezone) {
        User user = new UserImpl();
        user.setEmailAddress(emailAddress);
        user.setName(name);
        user.setMentionName(mentionName);
        user.setTitle(title);
        user.setAdmin(admin);
        user.setPassword(password);
        user.setTimezone(timezone);
        if(user.create()) {
            return user;
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        User user = get(id);
        return user.delete();
    }

    @Override
    public boolean undelete(int id) {
        User user = get(id);
        return user.undelete();
    }
    
    // ----------------------------------------------------------------------------------------------- Private Methods
    
    private static HipChatApi getConnection() {
        HipChatApi conn = HipChatApiImpl.getInstance();
        if(conn.getAuthToken().equals("")) {
            conn.setLastErrorException(new IllegalArgumentException(Error.REQUIRED_PARAMETER_AUTHTOKEN));
            throw (IllegalArgumentException) conn.getLastErrorException();
        }
        return conn;
    }
    
    
}
