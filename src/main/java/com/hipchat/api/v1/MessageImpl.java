
package com.hipchat.api.v1;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class MessageImpl implements Message {

    @SerializedName("date")     private Date date;
    @SerializedName("from")     private UserImpl author;
    @SerializedName("message")  private String message;
    
    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public UserImpl getAuthor() {
        return this.author;
    }

    @Override
    public void setAuthor(UserImpl author) {
        this.author = author;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
    
}
