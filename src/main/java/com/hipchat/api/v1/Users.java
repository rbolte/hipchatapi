
package com.hipchat.api.v1;

import java.util.List;
import java.util.TimeZone;

public interface Users {

    public User get(int id);
    public List<User> getList();
    public List<User> getList(boolean includeDeleted);
    
    public User create(String emailAddress, String name);
    public User create(String emailAddress, String name, String mentionName);
    public User create(String emailAddress, String name, String mentionName, String title);
    public User create(String emailAddress, String name, String mentionName, String title, boolean admin);
    public User create(String emailAddress, String name, String mentionName, String title, boolean admin, String password);
    public User create(String emailAddress, String name, String mentionName, String title, boolean admin, String password, TimeZone timezone);
    
    public boolean delete(int id);
    public boolean undelete(int id);
    
}
